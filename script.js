let name = document.querySelector(".name");
let email = document.querySelector(".email");
let message = document.querySelector(".message");
let submit = document.querySelector(".submit");
let status = document.querySelector(".status");

submit.addEventListener("click",function () {


    let ajax = new XMLHttpRequest();
    let data = new FormData();

    name = name.value;
    name = name.trim();

    email = email.value;
    email = email.trim();

    message = message.value;

    if(name == ""){
        status.textContent = "Please enter your name";
    }else {

        if(email == ""){
            status.textContent = "Please enter your email";
        }else {

            if(message == ""){
                status.textContent = "Please enter your message";
            }else {

                // Good to go
                this.disabled = true;
                status.textContent = "In progressing,please wait...";

                data.append("name",name);
                data.append("email",email);
                data.append("message",message);

                ajax.onreadystatechange = function () {

                    if(ajax.status == 200 && ajax.readyState == 4){


                        // AJAX is success
                        status.textContent = `Thanks for the feedback ${name},we are looking forward to contact you once again`;



                    }

                }

                ajax.open("POST","formProgress.php");
                ajax.send(data);

            }

        }

    }




});