<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>AJAX Form</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

    <form class="form">
        <p class="status"></p>
        <p>Name: <input type="text" class="name" required></p>
        <p>Email: <input type="email" class="email" required></p>
        <p>Message: <textarea class="message" required></textarea></p>
        <button class="submit">Send</button>
    </form>
    <script src="script.js"></script>
</body>
</html>